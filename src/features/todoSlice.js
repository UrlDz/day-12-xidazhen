import { createSlice } from "@reduxjs/toolkit";
const todoSlice = createSlice({
    name: 'todo',
    initialState: {
        todoList: []
    },
    reducers: {
        updateTodoList: (state, action) => {
            state.todoList.push(action.payload)
        },
        finishTodo: (state, action) => {
            const id = action.payload
            console.log(id);
            state.todoList.filter(everyTodo => everyTodo.id === id).map(todo => {
                console.log(todo.finished);
                todo.finished = !todo.finished;
            })
        },
        deleteTodo: (state, action) => {
            const { id } = action.payload
            state.todoList = state.todoList.filter(todo => todo.id !== id)
        },
    },
});
export default todoSlice.reducer;
export const { updateTodoList, finishTodo, deleteTodo } = todoSlice.actions;