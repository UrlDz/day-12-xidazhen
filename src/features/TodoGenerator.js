import { useState } from 'react';
import { useDispatch } from 'react-redux'
import { nanoid } from '@reduxjs/toolkit';
import { updateTodoList } from './todoSlice';

const TodoGenerator = () => {
  const [newTodo, setNewTodo] = useState('');
  const dispatch = useDispatch()
  const handleSubmit = (e) => {
    e.preventDefault();
    if (newTodo.trim()) {
      dispatch((updateTodoList({ text: newTodo, id: nanoid(), finished: false })))
      setNewTodo("")
    }

  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        value={newTodo}
        onChange={(e) => setNewTodo(e.target.value)}
      />
      <button type="submit">Add</button>
    </form>
  );
};

export default TodoGenerator;