import TodoItem from "./TodoItem";
import { useSelector } from 'react-redux'
const TodoGroup = () => {
    const todoList = useSelector(state => state.todo.todoList)
    
    return (
        <ul>
            {todoList.map((item) => (
                <TodoItem key={item.id} todo={item} />
            ))}
        </ul>
    );
};

export default TodoGroup;
