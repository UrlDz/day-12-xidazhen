import { useDispatch } from 'react-redux'
import { deleteTodo, finishTodo } from './todoSlice';

const TodoItem = ({ todo }) => {
    const dispatch = useDispatch()
    const handleDelete = () => {
        dispatch(deleteTodo(todo))
    }
    const handleFinish = () => {
        dispatch(finishTodo(todo.id))
    }


    return <li>
        <button onClick={handleDelete}>X</button>
        <label onClick={handleFinish}>
            {todo.finished ? <del>{todo.text}</del> : todo.text}
        </label>

    </li>;
};
export default TodoItem;