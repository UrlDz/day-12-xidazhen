
import React from 'react';
import TodoList from './features/TodoList';

const App = () => {
  return <TodoList />;
};

export default App;